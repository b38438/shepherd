# Shepherd Themes

Shepherd ships with a set of themes for you to choose from.

👀 Take a look!

<style>
  .theme-image {
    height: auto;
    max-width: 600px;
    width: 100%;
  }
</style>


#### Light (Default)

```js
const tour = new Shepherd.Tour({
  ...
  theme: 'light'
});
```

<img src="/docs-src/assets/img/themes/default.png" alt="Light Theme" class="theme-image"/>


#### Dark

```js
const tour = new Shepherd.Tour({
  ...
  theme: 'dark'
});
```

<img src="/docs-src/assets/img/themes/dark.png" alt="Dark Theme" class="theme-image"/>


#### Square Light

```js
const tour = new Shepherd.Tour({
  ...
  theme: 'squareLight'
});
```

<img src="/docs-src/assets/img/themes/square.png" alt="Square Light Theme" class="theme-image"/>


#### Square Dark

```js
const tour = new Shepherd.Tour({
  ...
  theme: 'squareDark'
});
```

<img src="/docs-src/assets/img/themes/square-dark.png" alt="Square Dark Theme" class="theme-image"/>
